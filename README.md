# Scripts for using dmenu for various purposes

## 1. dmenu_rundesktop

This is like the default `dmenu_run` which allows
you to execute any program residing in `$PATH`.
Instead `dmenu_rundesktop` parses `*.desktop` files in
`/usr/share/applications/` and builds a list
of executables from the desktop entries.
Desktop entries which have Terminal=true run with termite.

## 2. dmenu_exec

Use this like a tiny popup terminal for running short commands
for which you don't want to bother opening a new terminal window.
I use it for running scrot, changing wallpapers, starting/stopping tasks.
It gives suggestions from `~/.bash_history`.

## 3. dmenu_net

This is a menu for netcfg profiles which are found in `/etc/network.d/`
